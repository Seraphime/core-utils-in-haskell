{-
    This source code is under BSD-Clause 2.
        May it serve you as it can.
-}

module Main where

import Control.Monad
import System.IO
import Utils
import qualified Data.List as DL
import Data.Char ( isSpace )
import System.Exit ( exitSuccess, die )
import System.Environment ( getArgs )
import System.Posix.Files ( fileExist, fileAccess )
        
data OptFlag = None
             | Verbose
             | EndLine
             | Tabs
             | TabsEndLn
             | VerboseTabs
             | VerboseExt
             | VerboseTE
             | Numbering
             | NumberingSpaces
             | SkipEmpty
                deriving (Enum, Eq, Show)

type FlagsState = ( OptFlag, OptFlag, OptFlag )

instance Monoid OptFlag where
    mempty = None
    mappend = combVFlag

combVFlag :: OptFlag -> OptFlag -> OptFlag
combVFlag p q
    | p == None                              = q
    | q == None                              = p
    | p == Tabs && q == EndLine              = TabsEndLn
    | p == Verbose && q == EndLine           = VerboseExt
    | p == Verbose && q == Tabs              = VerboseTabs
    | p == Verbose                           = q
    | p == Numbering && q == NumberingSpaces = q
    | p == q                                 = p
    | otherwise                              = combVFlag q p

combVF :: FlagsState -> FlagsState -> FlagsState
combVF (p, q, r) (s, t, w) = (p `combVFlag` s, q `combVFlag` t, r `combVFlag` w)

getFlag :: Char -> OptFlag
getFlag 'A' = VerboseTE
getFlag 'E' = EndLine
getFlag 'e' = VerboseExt
getFlag 'v' = Verbose
getFlag 'T' = Tabs
getFlag 't' = VerboseTabs
getFlag 'b' = Numbering
getFlag 'n' = NumberingSpaces
getFlag 's' = SkipEmpty
getFlag b = quitWithError ("cat: unknown option -" ++ [b]) 

cat :: Handle -> ([String] -> [String])-> IO ()
cat h f = do
    contents <- hGetContents h
    mapM_ putStrLn $ f (lines contents)

mkVFlag :: OptFlag -> FlagsState
mkVFlag SkipEmpty       = (None, None, SkipEmpty)
mkVFlag Numbering       = (None, Numbering, None)
mkVFlag NumberingSpaces = (None, NumberingSpaces, None)
mkVFlag p               = (p, None, None)

getV :: OptFlag -> ([String] -> [String])
getV Tabs        = \s -> map ( concatMap (\c -> if c == '\t' then "^I" else [c]) ) s
getV EndLine     = \s -> map (++"$") s
getV VerboseTabs = getV EndLine . getV Tabs
getV None        = id
getV _           = id      -- Not implemented

getN :: OptFlag -> ([String] -> [String])
getN None               = id
getN Numbering          = Utils.numberLinesNoEmpty
getN NumberingSpaces    = Utils.numberLines

parseFlags :: String -> FlagsState
parseFlags []     = (None, None, None)
parseFlags (x:xs) = (mkVFlag ( getFlag x )) `combVF` (parseFlags xs)

catFiles :: ( [String] -> [String] ) -> FilePath -> IO ()
catFiles func f = do
  if f == "-" then cat stdin func else do
       canGo <- Utils.checkFile f
       case canGo of
        Go  -> do
              h <- openFile f ReadMode
              cat h func
              hClose h
        Error p -> hPutStrLn stderr $ p

getFunction :: FlagsState -> ( [String] -> [String] )
getFunction (v, n, skip)
    | skip == SkipEmpty = \ss -> f $ filter (all (not . isSpace) ) ss
    | otherwise         = f
        where
         f = ( getV v ) . ( getN n )

main = do
    args <- getArgs
    let (options, fpaths) = span isOption args
    let f = getFunction $ foldr combVF (None, None, None) $ map (parseFlags . tail) options
    when (length fpaths > 0) $
        mapM_ (catFiles f) fpaths >>
        exitSuccess
    cat stdin f
        where
        isOption :: String -> Bool
        isOption (s1:[])   = False
        isOption (s:s1:s2) = (s == '-') && if s1 == '-'
                        then not $ null s2
                        else True
