module Main where

import System.IO 
import System.Environment ( getArgs )
import Utils

tac :: ([String] -> [String] ) -> Handle -> IO ()
tac f h = do
        contents <- hGetContents h
        mapM_ putStrLn $ reverse (lines contents)

tacFiles :: ( [String] -> [String] ) -> FilePath -> IO ()
tacFiles func f = do
  if f == "-" then tac func stdin else do
       canGo <- Utils.checkFile f
       case canGo of
        Go  -> do
              h <- openFile f ReadMode
              tac func h
              hClose h
        Error p -> hPutStrLn stderr $ p

main = do
  args  <- getArgs
  mapM_ (tacFiles id) args
  
