{-
    This source code is under BSD-Clause 2.
        May it serve you as it can.
-}

module Utils     (
      Status(..), checkFile, numberLinesNoEmpty,
      numberLines, quitWithError
    ) where

import Data.Char ( isSpace )
import System.Posix.Files ( fileExist, fileAccess )
import System.IO.Unsafe ( unsafePerformIO )
import System.Environment ( getProgName )
import System.Exit ( die )
import qualified System.Directory as SD


data Status = Go
            | Error String

checkFile :: FilePath -> IO Status
checkFile f = do
  exists    <- fileExist f
  perms     <- fileAccess f True False False
  isDir     <- SD.doesDirectoryExist f
  progName  <- getProgName
  case (exists, perms, isDir) of
    (False, _, _)       -> return ( Error $ mkErrorMsg "No such file" )
    (_, False, _)       -> return ( Error $ mkErrorMsg "Permission denied" )
    (_, _, True)        -> return ( Error $ mkErrorMsg "Is directory" )
    _                   -> return Go
    where
      mkErrorMsg msg = f ++ ": " ++ msg

numberLinesNoEmpty :: [String] -> [String]
{-# INLINE numberLinesNoEmpty #-}
numberLinesNoEmpty ss = zipWith (\a b -> 
                        if not $ all isSpace b
                        then "\t" ++ show a ++ b
                        else b) [1..] ss

numberLines :: [String] -> [String]
{-# INLINE numberLines #-}
numberLines ss = zipWith (\a b -> "\t" ++ show a ++ "  " ++ b) [1..] ss

quitWithError msg = unsafePerformIO $ die msg
